<?php

use App\Http\Controllers\Bot\BotController;
use Illuminate\Support\Facades\Route;

Route::post('/message', [BotController::class, 'reply']);