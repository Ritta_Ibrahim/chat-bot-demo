<?php

namespace App\Http\Controllers\Bot;

use App\Http\Controllers\Controller;
use App\Services\BotService;
use Illuminate\Http\Request;

class BotController extends Controller
{
    public function reply(Request $request)
    {
        // cache()->forget('step');
        // die();

        $step = cache('step');
        // dd($step);
        $response = BotService::generateResponse($request->message, $step);

        // dd(cache());
        return response()->json($response);
    }
}
