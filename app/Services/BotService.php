<?php

namespace App\Services;

class BotService
{
    // public static function getOptions()
    // {
    //     return [
    //         1 => 'I lost my passward',
    //         2 => 'I want to reset my password',
    //         3 => 'I want to connect with someone from the company',
    //     ];
    // }

    // public static function getOptionsResponses()
    // {
    //     return [
    //         1 => 'Do you want to reset your password?',
    //         2 => 'please answer the following questions',
    //         3 => 'Here are the contact information: email: test@test.com phone:123456789',
    //     ];
    // }

    // public static function getQuestions()
    // {
    //     return [
    //         1 => "What is your fescal code?",
    //         2 => "What is your email?",
    //         3 => "Enter your verification code:",
    //     ];
    // }

    // public static function getGreetings()
    // {
    //     return [
    //         'welcome' => "Welcome to ecm chatbot, please choose one of the following options.",
    //         'error' => "An invalid data has entered, please try again or contact us.",
    //         'success' => "Your request has successfully done, you're welcome.",
    //     ];
    // }

    // const OPTIONS = 1;
    // const PASSWORD = 2;
    // const HELP = 3;

    // public static function generateResponse($message, $prev = "", $step = null)
    // {
    //     $res = [];
    //     $resMessage = "";

    //     if ($step) {
    //         if ($step == self::OPTIONS) {
    //             $key = array_search($message, self::getOptions());
    //             if ($key !== null) {
    //                 $resMessage = self::getOptionsResponses()[$key];
    //             }
    //             if ($key == 1) { // time to change to asking questions
    //                 $resMessage .= " \n " . self::getQuestions()[1];
    //                 $step = self::PASSWORD;
    //             }
    //         } else if ($step == self::PASSWORD) {
    //             if (str_contains($prev, self::getQuestions()[1])) {
    //                 dd('fescal');

    //             }
    //         } else if ($step == self::HELP) {
    //         }
    //     } else {
    //         $resMessage = self::getGreetings()['welcome'];
    //         $step = self::OPTIONS;
    //     }
    //     $res['message'] = $resMessage;
    //     $res['step'] = $step;
    //     return $res;
    // }

    const START = 1;
    const LOST_PASS = 2;
    const FESCAL = 3;
    const EMAIL = 4;
    const VERIY_CODE = 5;
    const SUCCESS = 6;
    const ERROR = 7;
    const CONTACT = 8;
    const INFO = 9;

    public static function getOptions()
    {
        return [
            self::START => "Welcome to ecm chatbot, please choose one of the following options. (answer: I lost my passward) or (answer: I want to connect with someone)", // null
            self::LOST_PASS => 'I lost my passward', //  next step 3
            self::FESCAL => 'What is your fescal code? (answer: fescal)', //  next step 4
            self::EMAIL => "What is your email? (answer: email)", //  next step 5
            self::VERIY_CODE => "Enter your verification code: (answer: code)", //  next step 6
            self::SUCCESS => "Your request has successfully done, you're welcome. (answer: start)", //  next step 1
            self::ERROR => "An invalid data has entered, please try again (answer: start)", //  next step 1
            self::CONTACT => 'I want to connect with someone',
            self::INFO => 'Here are the contact information: email: test@test.com phone:123456789 (answer: start)',
        ];
    }

    public static function generateResponse($message, $step = null)
    {
        $res = [];
        $resMessage = "";

        if ($message == "start") {
            $resMessage = self::getOptions()[self::START];
            $step = self::START;
            cache(['step' => $step], now()->addMinutes(5));
        } else
        if ($step) {

            // user sent: I lost my passward
            // bot sent: What is your fescal code?
            if (($step == self::START || $step == self::SUCCESS || $step == self::ERROR || $step == self::INFO)
                && str_contains(self::getOptions()[self::LOST_PASS], $message)) {

                $resMessage = self::getOptions()[self::FESCAL];
                $step = self::FESCAL;
                cache(['step' => $step], now()->addMinutes(5));
            }

            // user sent: I want to connect with someone
            // bot sent: Here are the contact information...
            if (($step == self::START || $step == self::SUCCESS || $step == self::ERROR || $step == self::INFO)
                && str_contains(self::getOptions()[self::CONTACT], $message)) {

                $resMessage = self::getOptions()[self::INFO];
                $step = self::INFO;
                cache(['step' => $step], now()->addMinutes(5));
            }

            // user send his fescal
            // bot sent: What is your email?
            else if ($step == self::FESCAL && $message == "fescal") {
                // get the user based on fexcal to make sure the user is exist
                // save the user id to the cache
                $resMessage = self::getOptions()[self::EMAIL];
                $step = self::EMAIL;
                cache(['step' => $step], now()->addMinutes(5));
            }

            // user send his email
            // bot sent: Enter your verification code:
            else if ($step == self::EMAIL && $message == "email") {
                // get the user by email
                // compare the user id with the cached one
                // send email to the user email which has verification code
                // save the code sent in cache
                $resMessage = self::getOptions()[self::VERIY_CODE];
                $step = self::VERIY_CODE;
                cache(['step' => $step], now()->addMinutes(5));
            }

            // user send the verification code
            // bot sent: Enter your verification code:
            else if ($step == self::VERIY_CODE && $message == "code") {
                // send another email to the user with reset password link
                $resMessage = self::getOptions()[self::SUCCESS];
                $step = self::SUCCESS;
                cache(['step' => $step], now()->addMinutes(5));
            } else {
                $resMessage = self::getOptions()[self::ERROR];
                $step = self::ERROR;
                cache(['step' => $step], now()->addMinutes(5));
            }

        } else {
            $resMessage = self::getOptions()[self::START];
            $step = self::START;
            cache(['step' => $step], now()->addMinutes(5));
        }
        $res['message'] = $resMessage;
        $res['step'] = $step;
        return $res;
    }

}
